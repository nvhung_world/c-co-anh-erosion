﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CoAnhErosion
{
    public partial class Erosion : Form
    {
        ImageType type;
        int mucXam;
        string FileName;
        public Erosion()
        {
            InitializeComponent();
            mucXam = 0;
            type = ImageType.Mau;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
        }
        private void MoAnhClick(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog1.FileName;
                LoadImage();
            }
        }
        private void LoadImage()
        {
            if (FileName == null)
                return;
            Enabled = false;
            try
            {
                Bitmap img = (Bitmap)Image.FromFile(FileName);
                if (type == ImageType.NhiPhan)
                {
                    for (int i = 0; i < img.Width; i++)
                        for (int j = 0; j < img.Height; j++)
                        {
                            Color c = img.GetPixel(i, j);
                            byte gray = Convert.ToByte(c.R * 0.287 + c.G * 0.599 + c.B * 0.114);
                            if (gray > 127)
                                img.SetPixel(i, j, Color.White);
                            else
                                img.SetPixel(i, j, Color.Black);
                        }
                }
                else if (type == ImageType.Xam)
                {
                    for (int i = 0; i < img.Width; i++)
                        for (int j = 0; j < img.Height; j++)
                        {
                            Color c = img.GetPixel(i, j);
                            byte gray = Convert.ToByte(c.R * 0.287 + c.G * 0.599 + c.B * 0.114);
                            img.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                        }
                }
                tabPage1.BackgroundImage = img;
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể lạp ảnh\n" + ex.Message, "Lỗi !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                button2.Enabled = false;
            }
            Enabled = true;
        }
        private void MT_KichThuong(object sender, EventArgs e)
        {
            int KichThuoc = int.Parse(comboBox2.Text);
            DataTable DT = (DataTable)dataGridView1.DataSource;
            if (DT == null)
                DT = new DataTable();
            while (DT.Columns.Count != KichThuoc)
                if (DT.Columns.Count < KichThuoc)
                {
                    DataColumn c = new DataColumn();
                    c.DataType = typeof(int);
                    c.AllowDBNull = false;
                    c.DefaultValue = 0;
                    c.ColumnName = (DT.Columns.Count +1).ToString();
                    DT.Columns.Add(c);
                }
                else
                    DT.Columns.RemoveAt(DT.Columns.Count - 1);
            while (DT.Rows.Count != KichThuoc)
                if (DT.Rows.Count < KichThuoc)
                    DT.Rows.Add();
                else
                    DT.Rows.RemoveAt(DT.Rows.Count - 1);
            dataGridView1.DataSource = DT;
            MT_Type(null, null);
        }
        private void MT_Type(object sender, EventArgs e)
        {
            DataTable DT = (DataTable)dataGridView1.DataSource;
            Random r = new Random();
            switch (comboBox1.SelectedIndex)
            {
                case 3:
                    for (int i = 0; i < DT.Rows.Count; i++)
                        for (int j = 0; j < DT.Columns.Count; j++)
                            DT.Rows[i][j] = mucXam;
                    break;
                case 0:
                    for (int i = 0; i < DT.Rows.Count; i++)
                        for (int j = 0; j < DT.Columns.Count; j++)
                            if (i == (DT.Rows.Count) / 2 || j == (DT.Columns.Count) / 2)
                                DT.Rows[i][j] = mucXam;
                            else
                                DT.Rows[i][j] = 0;
                    break;
                case 1:
                    for (int i = 0; i < DT.Rows.Count; i++)
                        for (int j = 0; j < DT.Columns.Count; j++)
                            if (j == i || DT.Rows.Count - 1 == i + j)
                                DT.Rows[i][j] = mucXam;
                            else
                                DT.Rows[i][j] = 0;
                    break;
                case 2:
                    for (int i = 0; i < DT.Rows.Count; i++)
                        for (int j = 0; j < DT.Columns.Count; j++)
                            if (j == i || DT.Rows.Count - 1 == i + j || i == (DT.Rows.Count) / 2 || j == (DT.Columns.Count) / 2)
                                DT.Rows[i][j] = mucXam;
                            else
                                DT.Rows[i][j] = 0;
                    break;
            }
        }
        private void CoAnhCLick(object sender, EventArgs e)
        {
            Enabled = false;
            DataTable MT = (DataTable)dataGridView1.DataSource;
            Bitmap Source = (Bitmap)tabPage1.BackgroundImage;
            Bitmap Result = new Bitmap(Source.Width, Source.Height);
            for (int i = 0; i < Source.Width; i++)
                for (int j = 0; j < Source.Height; j++)
                {
                    Color color;
                    if (type == ImageType.NhiPhan)
                    {
                        if (Source.GetPixel(i, j) == Color.White)
                            continue;
                        color = Color.Black;
                    }
                    else
                        color = Source.GetPixel(i, j);
                    for (int k = 0; k < MT.Columns.Count; k++)
                    {
                        bool br = false;
                        for (int l = 0; l < MT.Rows.Count; l++)
                        {
                            int DT_I = i + k - MT.Columns.Count / 2;
                            int DT_J = j + l - MT.Rows.Count / 2;
                            if (DT_I<0 || DT_I>= Source.Width || DT_J <0 || DT_J >= Source.Height)
                                continue;
                            if (type == ImageType.NhiPhan)
                            {
                                int color_MT = (int)MT.Rows[l][k],
                                    color_Source = Source.GetPixel(DT_I, DT_J).B;
                                if (color_MT != color_Source && color_MT == 0)
                                {
                                    color = Color.White;
                                    br = true;
                                    break;
                                }
                            }
                            else if (type == ImageType.Xam)
                            {
                                int color_MT = (int)MT.Rows[l][k],
                                    color_Source = Source.GetPixel(DT_I, DT_J).B;
                                int res = color_Source + color_MT;
                                if (res >= 255)
                                {
                                    color = Color.White;
                                    br = true;
                                    break;
                                }
                                color = res > color.B ? Color.FromArgb(res,res,res) : color;
                            }
                            else if(type == ImageType.Mau)
                            {
                                int color_MT = (int)MT.Rows[l][k],
                                    color_Source = Source.GetPixel(DT_I, DT_J).ToArgb();
                                int res = color_Source + color_MT;
                                if (res > -1)
                                {
                                    color = Color.White;
                                    br = true;
                                    break;
                                }
                                color = res > color.ToArgb() ? Color.FromArgb(res) : color;
                            }
                        }
                        if (br)
                            break;
                    }
                    Result.SetPixel(i,j,color);
                }
            panel1.BackgroundImage = Result;
            Enabled = true;
            button3.Enabled = true;
        }
        private void MucXamClick(object sender, EventArgs e)
        {
            mucXam = int.Parse(comboBox3.Text);
            MT_Type(null, null);
        }
        private void LuuAnhClick(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string FileName = saveFileDialog1.FileName;
                System.Drawing.Imaging.ImageFormat Format;
                switch(saveFileDialog1.FilterIndex)
                {
                    case 0:
                        Format = System.Drawing.Imaging.ImageFormat.Jpeg;
                        break;
                    case 1:
                        Format = System.Drawing.Imaging.ImageFormat.Bmp;
                        break;
                    default:
                        Format = System.Drawing.Imaging.ImageFormat.Png;
                        break;
                }
                panel1.BackgroundImage.Save(FileName, Format);
            }
        }
        private void CheDoXam_Changed(object sender, EventArgs e)
        {
            panel1.BackgroundImage = null;
            button2.Enabled = button3.Enabled = false;
            switch(comboBox4.SelectedIndex)
            {
                case 0:
                    type = ImageType.Mau;
                    comboBox3.Items.Clear();
                    comboBox3.Items.AddRange(new object[] { "0", "5", "10", "50", "100", "150", "200", "255" });
                    comboBox3.SelectedIndex = 0;
                    break;
                case 1:
                    type = ImageType.Xam;
                    comboBox3.Items.Clear();
                    comboBox3.Items.AddRange(new object[] { "0", "5", "10", "50", "100", "150", "200", "255" });
                    comboBox3.SelectedIndex = 0;
                    break;
                case 2:
                    type = ImageType.NhiPhan;
                    comboBox3.Items.Clear();
                    comboBox3.Items.Add("255");
                    comboBox3.SelectedIndex = 0;
                    break;
            }
            LoadImage();
        }
    }
}
